const mongoose = require('mongoose')

const FoodSchema = new mongoose.Schema({
    image: String,
    title: { type: String, required: true },
    description: String,
    price: Number,
    _company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    }
}, { timestamps: true })

mongoose.model('Food', FoodSchema)