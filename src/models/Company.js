const mongoose = require('mongoose')

const CompanySchema = new mongoose.Schema({
    image: String,
    title: { type: String, required: true }
}, { timestamps: true })

mongoose.model('Company', CompanySchema)