const express = require('express')
const routes = express.Router()

const FoodController = require('./controllers/FoodController')
routes.get('/food', FoodController.getAll)
routes.get('/food/:id', FoodController.getByID)
routes.get('/food/company/:id', FoodController.getByIDCompany)
routes.post('/food', FoodController.incluir)
routes.put('/food/:id', FoodController.editar)
routes.delete('/food/:id', FoodController.excluir)

const CompanyController = require('./controllers/CompanyController')
routes.get('/company', CompanyController.getAll)
routes.get('/company/:id', CompanyController.getByID)
routes.post('/company', CompanyController.incluir)
routes.put('/company/:id', CompanyController.editar)
routes.delete('/company/:id', CompanyController.excluir)

module.exports = routes