const mongoose = require('mongoose')
const Food = mongoose.model('Food')

mongoose.set('useFindAndModify', false)

module.exports = {
    async getAll(req, res) {
        var pageOptions = {
            page: parseInt(req.query.page) || 0,
            limit: parseInt(req.query.limit) || 5
        }

        await Food.find({}).populate('_company')
            .exec(function (err, reg) {
                const total = reg.length;

                let registros = [...reg]
                let pgInicial = pageOptions.page * pageOptions.limit;
                let pgFinal = pageOptions.limit + (pgInicial);
                registros = registros.slice(pgInicial, pgFinal)

                return res.json({ registros, total })
            })
    },
    async getByID(req, res) {
        const registro = await Food.findById(req.params.id).populate('_company');
        return res.json(registro)
    },
    async getByIDCompany(req, res) {
        var pageOptions = {
            page: parseInt(req.query.page) || 0,
            limit: parseInt(req.query.limit) || 2
        }

        // await Food.find({})
        //     // Faz o filtro pelo ID da empresa
        //     .populate('_company', null, { _id: { $eq: req.params.id } })
        //     // Faz paginar
        //     .skip(pageOptions.page * pageOptions.limit).limit(pageOptions.limit)
        //     // Percorre os registro e remove os NULOS
        //     .exec(function (err, reg) {
        //         return res.json(reg.filter((obj) => obj['_company'] != null))
        //     })

        await Food.find({}).populate('_company', null, { _id: { $eq: req.params.id } })
            .exec(function (err, reg) {
                reg = reg.filter((obj) => obj['_company'] != null)
                const total = reg.length;

                let registros = [...reg]
                let pgInicial = pageOptions.page * pageOptions.limit;
                let pgFinal = pageOptions.limit + (pgInicial);
                registros = registros.slice(pgInicial, pgFinal)

                return res.json({ registros, total })
            })
    },
    async incluir(req, res) {
        const registro = await Food.create(req.body);
        return res.json(registro)
    },
    async editar(req, res) {
        const registro = await Food.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(registro)
    },
    async excluir(req, res) {
        await Food.findByIdAndRemove(req.params.id);
        return res.send()
    }
}