const mongoose = require('mongoose')
const Company = mongoose.model('Company')

mongoose.set('useFindAndModify', false)

module.exports = {
    async getAll(req, res) {
        var pageOptions = {
            page: parseInt(req.query.page) || 0,
            limit: parseInt(req.query.limit) || 5
        }

        const registros = await Company.find({})
            .skip(pageOptions.page * pageOptions.limit)
            .limit(pageOptions.limit)

        return res.json(registros)
    },
    async getByID(req, res) {
        const registro = await Company.findById(req.params.id);
        return res.json(registro)
    },
    async incluir(req, res) {
        const registro = await Company.create(req.body);
        return res.json(registro)
    },
    async editar(req, res) {
        const registro = await Company.findByIdAndUpdate(req.params.id, req.body, { new: true });
        return res.json(registro)
    },
    async excluir(req, res) {
        await Company.findByIdAndRemove(req.params.id);
        return res.send()
    }
}