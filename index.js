const express = require('express')
const requireDir = require('require-dir')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

mongoose.connect('mongodb+srv://usuario:senha@banco-6jrp8.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const server = require('http').Server(app)

requireDir('./src/models')
app.use('/api', require('./src/routes'))

server.listen(process.env.PORT || 4000)